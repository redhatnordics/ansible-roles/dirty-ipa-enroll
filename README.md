dirty-ipa-enroll
================

A simple role that enrolls a client in to a IDM domain, not pretty in any way but gets the work done.

Currently needs to run on a enrolled server.

Role Variables
--------------

| variable     | default value |
| ------------ | ------------- |
| otp_password | Secret123     |




